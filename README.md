# Template that uses Soy, CSS, and JS #
* This is built around Stash (as this is the product I follow).
* Any input on how to make this better is welcome.  Please log an issue and I'll get to it as soon as I can.

### Initial Setup

- atlas-create-stash-plugin
- atlas-create-stash-plugin-module
-- Servlet
- atlas-create-stash-plugin-module
-- Component Import
--- com.atlassian.soy.renderer.SoyTemplateRenderer (no additional input)


### Other Info

- URL to servlet: http://localhost:7990/stash/plugins/servlet/myservlet
- Add Soy to Java and XML: https://developer.atlassian.com/stash/docs/latest/tutorials-and-examples/decorating-the-user-account.html

You have successfully created an Atlassian Plugin!

Here are the SDK commands you'll use immediately:

* atlas-run   -- installs this plugin into the product and starts it on localhost
* atlas-debug -- same as atlas-run, but allows a debugger to attach at port 5005
* atlas-cli   -- after atlas-run or atlas-debug, opens a Maven command line window:
                 - 'pi' reinstalls the plugin into the running product instance
* atlas-help  -- prints description for all commands in the SDK

Full documentation is always available at:

https://developer.atlassian.com/display/DOCS/Introduction+to+the+Atlassian+Plugin+SDK
