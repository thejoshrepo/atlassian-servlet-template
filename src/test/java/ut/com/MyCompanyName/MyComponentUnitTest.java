package ut.com.MyCompanyName;

import org.junit.Test;
import com.MyCompanyName.MyPluginComponent;
import com.MyCompanyName.MyPluginComponentImpl;

import static org.junit.Assert.assertEquals;

public class MyComponentUnitTest
{
    @Test
    public void testMyName()
    {
        MyPluginComponent component = new MyPluginComponentImpl(null);
        assertEquals("names do not match!", "myComponent",component.getName());
    }
}