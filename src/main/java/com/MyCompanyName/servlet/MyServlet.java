package com.MyCompanyName.servlet;

import com.atlassian.soy.renderer.SoyException;
import com.atlassian.soy.renderer.SoyTemplateRenderer;
import com.google.common.collect.ImmutableMap;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.*;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Map;

public class MyServlet extends HttpServlet{
    private static final Logger log = LoggerFactory.getLogger(MyServlet.class);
    private final SoyTemplateRenderer soyTemplateRenderer;

    public MyServlet(SoyTemplateRenderer soyTemplateRenderer){
        this.soyTemplateRenderer = soyTemplateRenderer;
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException
    {
        String templateName = "com.MyCompanyName.MyNamespace.FirstTemplateRendered";
        render(resp, templateName, ImmutableMap.<String, Object>builder()
                .put("passedParameter", "This was the \"passedParameter\" value.")
                .build()
        );
    }

    private void render(HttpServletResponse resp, String templateName, Map<String, Object> data) throws IOException, ServletException {
        resp.setContentType("text/html;charset=UTF-8");

        //Path to the templates: groupId.artifactId:resource-key
        try {
            soyTemplateRenderer.render(resp.getWriter(),"com.MyCompanyName.MyArtifactId:soy-templates", templateName, data);
        } catch (SoyException e) {
            Throwable cause = e.getCause();
            if (cause instanceof IOException) {
                throw (IOException) cause;
            }
            throw new ServletException(e);
        }
    }

}